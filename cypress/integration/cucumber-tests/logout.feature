Feature: Logout
  

  Scenario: Logout successfully from dashboard
    Given I am logged in
    And I go to the dashboard
    And I try to logout
    Then I should be logged out sucessfully