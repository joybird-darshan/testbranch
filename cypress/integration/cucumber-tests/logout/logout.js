import { Given, When, And, Then } from 'cypress-cucumber-preprocessor/steps';

Given('I am logged in', () => {
    cy.visit(Cypress.config().baseUrl)
    cy.get('.consumer-navbar__my-joybird')
        .click({force: true})
    cy.loginWith({
      email: 'darshan.singh+newjb1@joybird.com',
      password: 'testPASS66'
    })  
    cy.get('.consumer-login-button__main.consumer-navbar__list__anchor.logged-in-user')
    .should('contain', 'darsh')  
  });
  
  And('I go to the dashboard', () => {
    cy.get('.consumer-login-button__main.consumer-navbar__list__anchor.logged-in-user')
      .click()
  });
  
  And('I try to logout', () => {
    cy.get('button[type="submit"].Cta.Cta--color-brand.text-sm.Cta--noLinkPadding > .Cta__text.Cta__text-brand')
      .click()
  });

  Then('I should be logged out sucessfully', () => {
    cy.get('.consumer-navbar__my-joybird')
    .should('contain', 'My')  
  });