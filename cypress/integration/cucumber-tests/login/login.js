import { Given, When, And, Then } from 'cypress-cucumber-preprocessor/steps';

Given('I am at the home page', () => {
    cy.visit(Cypress.config().baseUrl)
});

When('I open the login form', () => {
  cy.get('.consumer-navbar__my-joybird')
      .click({force: true})
});

And('I enter my login details', () => {
  cy.loginWith({
    email: 'darshan.singh+newjb1@joybird.com',
    password: 'testPASS66'
  })
});

Then('I should be logged in sucessfully',()=>{
 
  cy.get('.consumer-login-button__main.consumer-navbar__list__anchor.logged-in-user')
    .should('contain', 'darsh') 
})

Given('I am at the sign-in page', () => {
  cy.visit(Cypress.config().baseUrl + 'sign-in/')
});

When('I log in', () => {
  cy.loginWith({
    email: 'darshan.singh+newjb1@joybird.com',
    password: 'testPASS66'
  })
});

Then('I am logged in successfully', () => {
  cy.reload(true)
  cy.get('.consumer-login-button__main.consumer-navbar__list__anchor.logged-in-user')
    .should('contain', 'darsh')
});

Given('I am at the login form', () => {
  cy.visit(Cypress.config().baseUrl)
  cy.get('.consumer-navbar__my-joybird')
      .click({force: true})
});

And('I log in with invalid password', () => {
  cy.loginWith({
    email: 'darshan.singh+newjb1@joybird.com',
    password: 'asdsadasd'
  })
});

Then('I am not able to log in', () => {
  cy.get('.LabeledFieldWrapper__error-message')
    .should('contain', 'Incorrect password')
});



