Feature: Login
  

  Scenario: Login successfully via sidebar
    Given I am at the home page
    When I open the login form
    And I enter my login details
    #And I try to login
    Then I should be logged in sucessfully

  Scenario: Login successfully via sign-on page
    Given I am at the sign-in page
    When I log in
    Then I am logged in successfully     

  Scenario: Login unsucessful with invalid password
    Given I am at the login form
    And I log in with invalid password
    Then I am not able to log in   
    