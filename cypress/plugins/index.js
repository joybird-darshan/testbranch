const cucumber = require('cypress-cucumber-preprocessor').default

module.exports = (on, config) => {
  on('file:preprocessor', cucumber())
  on('before:browser:launch', (browser = {}, launchOptions) => {
    if (browser.family === 'chromium' && browser.name !== 'electron') {
      launchOptions.args.push('--start-fullscreen')

      return launchOptions
    }

    if (browser.name === 'electron') {
      launchOptions.preferences.fullscreen = true

      return launchOptions
    }
  })
}