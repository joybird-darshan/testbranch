// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

// IGNORE ERROR

Cypress.Commands.add('ignore_error', () => {
    cy.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })
})

Cypress.Commands.add('loginWith', ({ email, password }) =>
    cy.get('input[name="email"][type="text"]')
      .type(email)
      .get('button[type="submit"].Cta.Cta--color-black')
      .click()
      .get('input[name="password"]')
      .type(password)
      .get('button[type="submit"].Cta.Cta--color-black')
      .click()
)